"""
@see http://confluence.atlassian.com/display/BITBUCKET/Issues


Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.request, urllib.parse, urllib.error, urllib.parse, re, json, ssl

from bs4 import BeautifulSoup
from .__init__ import *

class BitbucketData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Bitbucket: no id")

        # from the "visual" URL get the api URL
        uri = "https://api.bitbucket.org/2.0/repositories/%(user)s/%(project)s/issues/%(id)s" % id
        context = ssl.create_default_context(capath=CAPATH)
        data = json.load(urllib.request.urlopen(uri, context=context))

        self.status = data['state'] or failwith(uri, "Bitbucket", exn=NoStatusExn)
        self.resolution = None

        if self.status == 'duplicate':
            raise DupeExn(uri)


class RemoteBitbucket(RemoteBts):
    def __init__(self, cnf):
        RemoteBts.__init__(self, cnf, None, None, BitbucketData)

    # override base class method to extract the ID as useful as for _getUri
    def extractBugid(self, uri):
        bugre = re.compile(r"https?://(?:www.)?bitbucket.org/(?P<user>.*)/(?P<project>.*)/issues?/(?P<id>[0-9]+).*")
        res = bugre.match(uri)
        if res:
            return res.groupdict()
        else:
            return None

    # return a meaningful uri
    def _getUri(self, bugId):
        return "http://bitbucket.org/%(user)s/%(project)s/issue/%(id)s" % bugId

    def isClosing(self, status, resolution):
        return status in ('resolved', 'closed')

    def isWontfix(self, status, resolution):
        return status in ('wontfix',)


RemoteBts.register('bitbucket', RemoteBitbucket)
