"""
@see http://projects.edgewall.com/trac/wiki/TracTickets


Copyright 2006 Sanghyeon Seo <sanxiyn@gmail.com>
Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import re, urllib.request, urllib.parse, urllib.error, csv

from bs4 import BeautifulSoup
from .__init__ import *

any = re.compile(r'.*')


class TracTicket:
    def __init__(self, page, uri):
        try:
            list = [ x + '\n' for x in page.split('\n')]
            data = csv.reader(list, delimiter = '\t')
            
            keys = next(data)
            vals = next(data)
            dct  = dict((keys[i], vals[i]) for i in range(0, len(keys)))
            
            self.status     = dct['status']
            self.resolution = dct['resolution']
            self.id         = int(dct['id'])
            try :
                self.reporter   = dct['reporter']
                self.owner      = dct['owner']
                self.summary    = dct['summary']
                self.milestone  = dct['milestone']
                self.version    = dct['version']
                self.priority   = dct['priority']
                self.description = dct['description'].replace("\\r\\n", "\r\n")
            except KeyError :
                pass
        except:
            soup = BeautifulSoup(page, features="html5lib")

            status = soup.find(any, 'status')
            if status:
                self.status, self.resolution = self._process_status(status.strong.text)
            else:
                try:
                    # Trac 0.8.x
                    self.status = soup.find(any, dict(headers='h_status')).text
                    resolution  = soup.find(any, dict(headers='h_resolution')).text
                    self.resolution = (resolution != '&nbsp;' and resolution) or None
                except:
                    failwith(uri, "Probably error page")

    def _process_status(self, text):
        words = text.split()
        assert len(words) in (1, 2), "invalid status/resolution: %r" % words
        if len(words) == 1:
            return text, None
        else:
            return [x.strip('()') for x in words]


class TracData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Trac: no id")

        page = wget(uri + '?format=tab').replace('\ufeff', '')

        ticket = TracTicket(page, uri)

        self.status = ticket.status
        self.resolution = ticket.resolution

        if not ticket.status:
            failwith(uri, "Trac", exn=NoStatusExn)

        if ticket.resolution == "duplicate":
            raise DupeExn(uri)


class RemoteTrac(RemoteBts):
    def __init__(self, cnf):
        bugre  =  r"^%(uri)s/ticket/([0-9]+)$"
        urifmt = "%(uri)s/ticket/%(id)s"
        RemoteBts.__init__(self, cnf, bugre, urifmt, TracData)

    def isClosing(self, status, resolution):
        return status == 'closed' and resolution != 'wontfix'

    def isWontfix(self, status, resolution):
        return resolution == 'wontfix'

RemoteBts.register('trac', RemoteTrac)
