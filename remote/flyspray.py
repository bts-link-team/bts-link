"""
Copyright 2009 Ryan Niebur <ryanryan52@gmail.com>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.request, urllib.parse, urllib.error, urllib.parse, ssl

from bs4 import BeautifulSoup
from .__init__ import *

def parse_table(soup):
    return soup.first('td', attrs={'headers' : 'status'}).string.strip()

class FlySprayData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "FlySpray: no id")

        context = ssl.create_default_context(capath=CAPATH)
        soup = BeautifulSoup(urllib.request.urlopen(uri, context=context), features="lxml")

        self.status = parse_table(soup) or failwith(uri, "FlySpray", exn=NoStatusExn)
        closeddiv = soup.first('div', attrs={'id' : 'taskclosed'})
        if closeddiv:
            self.resolution = closeddiv.contents[7].strip()[6:]
        else:
            self.resolution = None

        if self.resolution == 'Duplicate':
            raise DupeExn(uri)

class RemoteFlySpray(RemoteBts):
    def __init__(self, cnf):

        bugre  = r"^%(uri)s/index.php?(?:.*&)?task_id=([0-9]+)(?:&.*)?$"
        urifmt = "%(uri)s/index.php?do=details&task_id=%(id)s"

        RemoteBts.__init__(self, cnf, bugre, urifmt, FlySprayData)

    def isClosing(self, status, resolution):
        return status == 'Closed' and resolution == 'Fixed'

    def isWontfix(self, status, resolution):
        return status == "Incomplete" or (status == 'Closed' and resolution in ('Will-Not-Implement', 'Invalid', 'Works-for-me'))

RemoteBts.register('flyspray', RemoteFlySpray)
