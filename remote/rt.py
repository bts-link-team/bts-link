"""
@see http://wiki.bestpractical.com/index.cgi?ManualIntroduction


Copyright 2006 Sanghyeon Seo <sanxiyn@gmail.com>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.request, urllib.parse, urllib.error, urllib.parse, ssl

from bs4 import BeautifulSoup
from .__init__ import *


def parse_table_rt4(soup, key):
    cell = soup.find_all(string=key)[0].find_parent('td')
    return cell.find_next_sibling('td').string


class RT4Data:
    def __init__(self, uri, id):
        context = ssl.create_default_context(capath=CAPATH)
        soup = BeautifulSoup(urllib.request.urlopen(uri, context=context), features="lxml")

        self.id = id or failwith(uri, "RT: no id")
        self.status = parse_table_rt4(soup, 'Status:') or failwith(uri, "RT", exn=NoStatusExn)
        self.resolution = None


class RT5Data:
    def __init__(self, uri, id):
        context = ssl.create_default_context(capath=CAPATH)
        soup = BeautifulSoup(urllib.request.urlopen(uri, context=context), features="lxml")

        self.id = id or failwith(uri, "RT: no id")
        self.status = soup.find_all(attrs={'class': 'status'})[0].find_all(attrs={'class': 'current-value'})[0].string or failwith(uri, "RT", exn=NoStatusExn)
        self.resolution = None


class RemoteRT4(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/Public/Bug/Display.html\?id=([0-9]+)$'
        urifmt = '%(uri)s/Public/Bug/Display.html?id=%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, RT4Data)

    def isClosing(self, status, resolution):
        return status in ('resolved', 'deleted')

    def isWontfix(self, status, resolution):
        return status in ('rejected',)


class RemoteRT5(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/Public/Bug/Display.html\?id=([0-9]+)$'
        urifmt = '%(uri)s/Public/Bug/Display.html?id=%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, RT5Data)

    def isClosing(self, status, resolution):
        return status in ('resolved', 'deleted')

    def isWontfix(self, status, resolution):
        return status in ('rejected',)


RemoteBts.register('rt4', RemoteRT4)
RemoteBts.register('rt5', RemoteRT5)
