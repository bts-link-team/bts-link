"""
@see http://developer.github.com/v3/issues/


Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.request, urllib.error, urllib.parse, urllib.parse, re, json, ssl

from bs4 import BeautifulSoup
from .__init__ import *
from .secrets import SECRETS

TOKEN = SECRETS['github']

class GithubData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Github: no id")

        context = ssl.create_default_context(capath=CAPATH)

        # from the "visual" URL get the api URL
        uri = uri.replace('https://github.com', 'https://api.github.com/repos')
        # needed to support querying pull requests too
        uri = uri.replace('/pull/', '/pulls/')
        req = urllib.request.Request(uri, headers = { 'Authorization': 'token %s' % TOKEN })
        data = json.load(urllib.request.urlopen(req, context=context))

        self.status = data['state'] or failwith(uri, "Github", exn=NoStatusExn)
        self.resolution = None

class RemoteGithub(RemoteBts):
    def __init__(self, cnf):
        RemoteBts.__init__(self, cnf, None, None, GithubData)

    # override base class method to extract the ID as useful as for _getUri
    def extractBugid(self, uri):
        bugre = re.compile(r"https://github.com/(?P<user>.*)/(?P<project>.*)/(?P<type>.*)/(?P<id>[0-9]+)(?P<comment>#issuecomment-[0-9]+)?$")
        res = bugre.match(uri)
        if res:
            return res.groupdict()
        else:
            return None

    # return a meaningful uri
    def _getUri(self, bugId):
        return "https://github.com/%(user)s/%(project)s/%(type)s/%(id)s" % bugId

    def isClosing(self, status, resolution):
        return status in ('closed',)

RemoteBts.register('github', RemoteGithub)
