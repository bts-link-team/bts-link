"""
Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.parse, subprocess

from bs4 import BeautifulSoup
from .__init__ import *
from .base import maketoken

def get_value(table, key):
    td = table.firstText(key).findParent('td')
    return td.contents[-1].string.strip()

class GForgeData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "GForge: no id")

        soup  = BeautifulSoup(wget(uri), features="lxml")
        table = soup.first('table', {'cellpadding': '0', 'width': '100%', 'border': None})
        #print table.prettify()
        self.status     = get_value(soup, 'State:') or failwith(uri, "GForge", exn=NoStatusExn)
        self.resolution = get_value(soup, 'Resolution: ') or None

        if self.resolution == 'Duplicate':
            raise DupeExn(uri)

class RemoteGForge(RemoteBts):
    """ GForge bugtracker """
    def __init__(self, cnf):
        
        # Default regexp matching the bug # as an id
        bugre  = r'^%(uri)s/(?:index.php|)\?(?:.*&)*aid=([0-9]+).*$'
        # format of canonical URL of bug (reusing 'uri' from config)
        urifmt = '%(uri)s/?aid=%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, GForgeData)

    def isClosing(self, status, resolution):
        return status == 'Closed' and resolution != 'Wont-Fix'

    def isWontfix(self, status, resolution):
        return resolution == 'Wont-Fix'

RemoteBts.register('gforge', RemoteGForge)


