"""
@see https://docs.gitlab.com/ee/api/issues.html


Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.request, urllib.error, urllib.parse, urllib.parse, re, json, ssl

from bs4 import BeautifulSoup
from .__init__ import *

class GitlabData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Gitlab: no id")

        context = ssl.create_default_context(capath=CAPATH)
        req = urllib.request.Request(uri+'?format=json')
        data = json.load(urllib.request.urlopen(req, context=context))

        self.status = data['state'] or failwith(uri, "Gitlab", exn=NoStatusExn)
        self.resolution = None

class RemoteGitlab(RemoteBts):
    def __init__(self, cnf):
        RemoteBts.__init__(self, cnf, None, None, GitlabData)

    # override base class method to extract the ID as useful as for _getUri
    def extractBugid(self, uri):
        bugre = re.compile(r"(?P<domain>.*)/(?P<group>.*)/(?P<project>.*)/(?P<type>.*)/(?P<id>[0-9]+)/?$")
        res = bugre.match(uri)
        if res:
            return res.groupdict()
        else:
            return None

    # return a meaningful uri
    def _getUri(self, bugId):
        return "%(domain)s/%(group)s/%(project)s/%(type)s/%(id)s" % bugId

    def isClosing(self, status, resolution):
        return status in ('closed', 'merged')

RemoteBts.register('gitlab', RemoteGitlab)
