"""
Copyright 2009 Clint Adams <schizo@debian.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.request, urllib.parse, urllib.error, urllib.parse, ssl

from bs4 import BeautifulSoup
from .__init__ import *

def parse_table(soup, key):
    cell = soup.find(attrs={'class': key}).find_next(attrs={'class': 'value'})
    return cell.string

class RedmineData:
    def __init__(self, uri, id):
        try:
            context = ssl.create_default_context(capath=CAPATH)
            soup = BeautifulSoup(urllib.request.urlopen(uri, context=context), features="lxml")
        except:
            context = ssl.create_default_context()
            context.check_hostname = False
            context.verify_mode = ssl.CERT_NONE
            soup = BeautifulSoup(urllib.request.urlopen(uri, context=context), features="lxml")

        self.id = id or failwith(uri, "Redmine: no id")
        self.status = parse_table(soup, 'status') or failwith(uri, "Redmine", exn=NoStatusExn)
        self.resolution = None

class RemoteRedmine(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/issues/([0-9]+)$'
        urifmt = '%(uri)s/issues/%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, RedmineData)

    def isClosing(self, status, resolution):
        return status in ('Closed',)

    def isWontfix(self, status, resolution):
        return status in ('Rejected',)

RemoteBts.register('redmine', RemoteRedmine)
