"""
https://hg.prosody.im/issue-tracker/file/tip/doc/api.markdown


Copyright 2018 Kim Alvefur <zash@zash.se>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
from bs4 import BeautifulSoup
from .__init__ import *

class ProsodyIssueData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "ProsodyIssues: no id")

        soup = BeautifulSoup(wget(uri), features="lxml")

        self.status = None
        self.resolution = None

        # The statues of a ticket is easily obtainable by parsing the classes associated with the `body` tag:
        # - `issue-<open, closed> identifies the general status of the issue --> map to `status`
        # - issue-tagged-Status-<tag> contains a much wider range of states an issue can be in --> map to `resolution`

        for cls in soup.find_all("body")[0]['class']:
            tokens = cls.lower().split('-')
            if len(tokens) == 2:
                self.status = tokens[-1]
            elif len(tokens) == 4 and tokens[2] == "status":
                self.resolution = tokens[-1]

        if self.resolution == "duplicate":
            raise DupeExn(uri)

class RemoteProsodyIssues(RemoteBts):
    """
    RemoteProsodyIssues({'uri':'https://issues.prosody.im/'})
    """

    def __init__(self, cnf, extractRe = None, uriFmt = None):
        if extractRe is None:
            extractRe = r"/(?P<id>[0-9]+)$"
        if uriFmt is None:
            uriFmt = r"%(uri)s/%(id)s"
        RemoteBts.__init__(self, cnf, extractRe, uriFmt, ProsodyIssueData)

    # status is either open or closed
    def isClosing(self, status, resolution):
        return status in ('closed', )

    def isWontfix(self, status, resolution):
        return resolution in ('WontFix', 'Invalid')

RemoteBts.register('prosody', RemoteProsodyIssues)
