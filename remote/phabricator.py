"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

from bs4 import BeautifulSoup
from .__init__ import *

class PhabricatorData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Debbugs: no id")

        soup = BeautifulSoup(wget(uri), features="lxml")

        # this is the small "box" right below the issue title in the header
        cell = soup.find_all("span", attrs={'class': 'phui-tag-core'})[0]

        self.status, self.resolution = cell.text.split(', ')

class RemotePhabricator(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/(T[0-9]+)$'
        urifmt = '%(uri)s/%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, PhabricatorData)

    def isClosing(self, status, resolution):
        return status in ('Closed',)

RemoteBts.register('phabricator', RemotePhabricator)
