"""
Manage the configuration file

Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""


import os, re, stat, time, urllib.request, urllib.parse, urllib.error, pathlib
from .sender import get_email
import yaml

class NoSuchSection(Exception):
    def __init__(self, section):
        self.args = (section)

class _Config(object):
    def __init__(self):
        with open(pathlib.Path(__file__).parent.parent / 'btslink.yaml') as y:
            self.__data = yaml.load(y, Loader=yaml.BaseLoader)

    def get(self, section, option):
        if section in self.__data and option in self.__data[section]:
            return self.__data[section][option]
        if option in self.__data['general']:
            return self.__data['general'][option]
        return None

    def spool(self):
        return self.__data['general']['spool']

    def cookies(self):
        return self.__data['general']['cookies']

    def ldap(self):
        return self.__data['general']['ldap']

    def resources(self):
        """returns the supported bugtrackers configurations values as a dict
        with keys the project name and value another dict with that BTS infon"""
        res = {}
        for btstype in self.__data['remotes']:
            for bts in self.__data['remotes'][btstype]:
                remote = self.__data['remotes'][btstype][bts]
                remote['type'] = btstype
                remote['uri'] = remote['uri'].rstrip('/')
                if 'uri-re' in remote:
                    remote['uri-re'] = re.compile(remote['uri-re'])
                if 'closing' in remote:
                    remote['closing'] = [s.strip() for s in
                                         remote['closing'].split(',')]
                res[bts] = remote
        return res

    def sender(self):
        if 'from' in self.__data['general']:
            return self.__data['general']['from']
        else:
            return get_email()[1]

    def replyTo(self):
        return self.__data['general']['reply-to']

class _Opener(urllib.request.FancyURLopener):
    version = "btslink"

    def __init__(self):
        urllib.request.FancyURLopener.__init__(self)
        self.addheader('Accept', '*/*')

BTSLConfig = _Config()
urllib.request._urlopener = _Opener()

