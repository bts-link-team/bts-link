#!/usr/bin/python3
"""
Update RRD with the last execution summary

Docs used: http://oss.oetiker.ch/rrdtool/doc/rrdupdate.en.html


Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import rrdtool
import os.path
import sys
from collections import defaultdict
import yaml

if len(sys.argv) == 1:
    print(("Usage: " + sys.argv[0] + " <summary files>"))
    sys.exit(1)

basedir = os.path.dirname(os.path.abspath(__file__))
rrdfile = os.path.join(basedir, 'bts-link.rrd')

r = defaultdict(lambda: defaultdict(dict))

with open(sys.argv[1]) as f:
    summary = yaml.load(f, Loader=yaml.FullLoader)
    # rrd wants epoch as timestamps
    summary_epoch = summary['Execution complete'].strftime('%s')
    r[summary_epoch]['elapsed_time'] = int(summary['Elapsed time'])
    for tag in summary['Tags summary']:
        r[summary_epoch][tag] = summary['Tags summary'][tag]

for ts in sorted(r.keys()):
    rrdtool.update(rrdfile,
        '-t', ':'.join(sorted(r[ts].keys())),
        '%s:%s' % (ts, ':'.join(str(r[ts][k]) for k in sorted(r[ts].keys())))
        )