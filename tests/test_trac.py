"""
Copyright 2008 Jelmer Vernooij <jelmer@samba.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import re
import pytest

from remote import trac


@pytest.mark.offline
def test_tabformat():
    ticket = trac.TracTicket("""id\tsummary\treporter\towner\tdescription\ttype\tstatus\tpriority\tmilestone\tcomponent\tversion\tresolution\tkeywords\tcc
42\tWe should not foo\tanonymous\tsomebody\tThis is a long description\\r\\nAnd it includes whitespace stuff.\tdefect\tclosed\tnormal\t3.0.2\tcore\t3.0\tfixed\t\t""", "https://example.com")
    assert ticket.id == 42
    assert ticket.summary == "We should not foo"
    assert ticket.milestone == "3.0.2"
    assert ticket.version == "3.0"
    assert ticket.reporter == "anonymous"
    assert ticket.owner == "somebody"
    assert ticket.description == "This is a long description\r\nAnd it includes whitespace stuff."
    assert ticket.status == "closed"
    assert ticket.priority == "normal"


@pytest.mark.offline
def test_fromhtml():
    ticket = trac.TracTicket("""
    <html><body>
    <span class="status"><strong>(closed fixed)<strong></span>
    </body></html>
""", "https://example.com")
    assert "closed" == ticket.status
    assert "fixed" == ticket.resolution


@pytest.mark.offline
def test_fromhtml_noresolution():
    ticket = trac.TracTicket("""
    <html><body>
    <span class="status"><strong>open<strong></span>
    </body></html>
""", "https://example.com")
    assert "open" == ticket.status
    assert ticket.resolution is None

def test_tracdata():
    bug = trac.TracData("http://trac.cppcheck.net/ticket/10668", 1002531)
    assert bug.status == 'new'


def test_remotetrac():
    remote = trac.RemoteTrac({'uri': 'http://trac.cppcheck.net', 'uri-re': re.compile('https?://trac.cppcheck.net/ticket/([0-9]+)'), 'type': 'trac'})
    assert remote.extractBugid("http://trac.cppcheck.net/ticket/10668") == "10668"