"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import pytest


@pytest.mark.xfail(reason="no active remotes associated with this remote, remove?")
def test_gnats():
    assert False

