"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import re
from remote import debbugs


def test_debbugsdata():
    bug = debbugs.DebbugsData("https://debbugs.gnu.org/cgi/bugreport.cgi?bug=61880", 61880)
    assert bug.status == 'done'


def test_remotedebbugs():
    remote = debbugs.RemoteDebbugs({'uri': 'https://debbugs.gnu.org', 'uri-re': re.compile('https?://debbugs.gnu.org/(?:cgi/bugreport.cgi\\?bug=|)([0-9]+)$'), 'type': 'debbugs'})
    assert remote.extractBugid("https://debbugs.gnu.org/cgi/bugreport.cgi?bug=61880") == '61880'
