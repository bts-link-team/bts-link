"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import re

from remote import jira


def test_jiradata():
    bug = jira.JIRAData("https://jira.mariadb.org/browse/MDEV-29644", 1034889)
    assert bug.status == 'Closed'


def test_remotejira():
    remote = jira.RemoteJIRA({'uri': 'https://jira.mariadb.org', 'type': 'jira'})
    assert remote.extractBugid("https://jira.mariadb.org/browse/MDEV-29644") == "MDEV-29644"
