"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import re

from remote import roundup


def test_roundupedata():
    bug = roundup.RoundupData("https://bugs.python.org/issue42580", 42580   )
    assert bug.status == 'closed'
    assert bug.resolution == 'not a bug'


def test_remoteroundup():
    remote = roundup.RemoteRoundup({'closing': ['closed'], 'uri': 'https://bugs.python.org', 'wontfix': 'wont-fix', 'type': 'roundup'})
    assert remote.extractBugid("https://bugs.python.org/issue42580") == "42580"
