"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import pytest
import re

from remote import base, github


@pytest.mark.offline
def test_maketoken():
    assert base.maketoken('NONE') is None
    assert base.maketoken('not none') == 'not-none'


@pytest.mark.offline
def test_warn(capsys):
    msg = "This is a warning message"
    base.warn(msg)
    assert msg in capsys.readouterr().err


@pytest.mark.offline
def test_remotebts():

    remote = base.RemoteBts({'bugfmt': 'https://fake-url/%s', 'uri-re': re.compile('https://fake-url/([0-9]+)')})

    remote.enqueue(123)
    assert 123 in remote._queue

    uri = remote.getUri('123')
    assert uri.endswith('123')

    bugid = remote.extractBugid('https://fake-url/123')
    assert bugid == '123'


def test_remotereport():
    uri = "https://github.com/puddletag/puddletag/issues/468"
    data = github.GithubData(uri,
                             {'user': 'puddletag', 'project': 'puddletag', 'type': 'issues', 'id': '468', 'comment': None})
    bts = github.RemoteGithub({'uri': 'https://github.com', 'type': 'github'})
    remotereport = base.RemoteReport(data, bts)
    assert remotereport.status == 'closed'
    assert remotereport.closed
    assert remotereport.wontfix is None
    assert remotereport.uri == uri
