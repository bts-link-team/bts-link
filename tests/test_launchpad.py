"""
Copyright 2008 Jelmer Vernooij <jelmer@samba.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import pytest
from remote import launchpad


@pytest.mark.offline
def test_launchpadbug_id():
    bug = launchpad.LaunchpadBug("""bug: 42
reporter: Somebody
title: The bar is broken
""")
    assert bug.id == 42
    assert bug.reporter == "Somebody"
    assert bug.title == "The bar is broken"


@pytest.mark.offline
def test_launchpadbug_duplicate():
    bug = launchpad.LaunchpadBug("""bug: 42
reporter: Somebody
title: The bar is broken
duplicate-of: 43
""")
    assert bug.duplicate_of == 43


def test_launchpad_tasks():
    """
    verify we can parse tasks, one by one
    """
    bug = launchpad.LaunchpadBug("""bug: 42
reporter: Somebody
title: The bar is broken

task: task1
status: open

task: task2
status: open
""")
    assert sorted(bug.tasks.keys()) == ['task1', 'task2']


def test_launchpaddata():
    bug = launchpad.LaunchpadData("https://bugs.launchpad.net/bugs/1931174")
    assert bug.status == 'Fix Released'


def test_remoteredmine():
    remote = launchpad.RemoteLaunchpad({'uri': 'https://bugs.launchpad.net', 'type': 'launchpad'})
    assert remote.extractBugid("https://bugs.launchpad.net/bugs/1931174") == "1931174"
