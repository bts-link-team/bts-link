"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
from remote import gitlab


def test_gitlabdata():
    bug = gitlab.GitlabData("https://gitlab.com/NTPsec/ntpsec/-/issues/785", "1033088")
    assert bug.status == 'opened'


def test_remotegitlab():
    remote = gitlab.RemoteGitlab({'uri': 'https://gitlab.com', 'type': 'gitlab'})
    assert remote.extractBugid("https://gitlab.com/NTPsec/ntpsec/-/issues/785")['id'] == '785'