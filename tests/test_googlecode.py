"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import pytest


@pytest.mark.xfail(reason="This is expected, since bugs.chromium.org/crbug.com uses JS to load the actual content of the page...")
def test_googlecode():
    assert False

