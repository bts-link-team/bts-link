"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import re

from remote import savane


def test_savanedata():
    bug = savane.SavaneData("https://savannah.gnu.org/bugs/index.php?62648", 1030318)
    assert bug.status == 'Closed'


def test_remotesavane():
    remote = savane.RemoteSavane({'uri': 'http://savannah.gnu.org/bugs', 'uri-re': re.compile('https?://savannah.gnu.org/bugs/(?:index.php|)\\?(?:(?:.*&)*item_id=)?([0-9]+)'), 'type': 'savane'})
    assert remote.extractBugid("https://savannah.gnu.org/bugs/index.php?62648") == '62648'
