"""
Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import ldap, sys, os, re, subprocess

""" @package interfaces """

###############################################################################
# UserTags stuff

# TODO: add to dep for DSA services
import debianbts
from collections import defaultdict

_usertags = defaultdict(list)
def _parseUserTags(cnf):
    """
    Parses a dictionary, containing tags -> list of bugs with that tag to a
    "cache" dictionary of bugs -> list of tags associated with it.

    :param cnf: bts-link configuration (needed for the BTS user)
    :return: nothing, updates _usertags
    """
    tags = debianbts.get_usertag(cnf.get('general', 'user'))
    for tag, bugs in tags.items():
        for bug in bugs:
            _usertags[str(bug)].append(tag)


###############################################################################
# Reports Stuff


class BtsReport:
    """ a debbugs bug report
    
    Attributes :
        id : bug id
        usertags : usertags like 'status-xxx' or 'resolution-xxx'
        forward : list of 'Forwarded-To' URIs
        merge : list of 'merged-upstream:' URIs
        fwdTo : "raw" list of 'Forwarded-To' values
    """
    def __init__(self, id):
        """ Constructor
        
            @param id: bug id
        """
        self.id = id
        if id in _usertags:
            self.usertags = _usertags[id]
        else:
            self.usertags = []

        self.forward = None
        self.merge   = None

        if len(self.fwdTo) == 1:
            self.forward = self.fwdTo[0]
            self.merge   = None

        # In such cases, it means that we know that the 2 remote upstream bugs have been merged upstream
        if len(self.fwdTo) == 2 and self.fwdTo[1].startswith('merged-upstream: '):
            self.forward = self.fwdTo[0].strip()
            self.merge   = self.fwdTo[1][len('merged-upstream: '):].strip()

    def package(self):    pass
    def srcPackage(self): pass

    def _usertagValue(self, key):
        prefix = key + '-'
        for t in self.usertags:
            if t.startswith(prefix):
                return t[len(prefix):]
        return None

    def remoteStatus(self):
        """returns usertag in the form of status-..."""
        return self._usertagValue('status')

    def remoteResolution(self):
        """returns usertag in the form of resolution-..."""
        return self._usertagValue('resolution')

class BtsLdapReport(BtsReport):
    def __init__(self, id, ldapdata):
        self.tags       = self._get('debbugsTag', ldapdata)
        self.fwdTo      = self._get('debbugsForwardedTo', ldapdata)
        self.package    = ldapdata['debbugsPackage'][0]
        self.srcpackage = ldapdata['debbugsSourcePackage'][0]
        self.done       = ldapdata['debbugsState'][0].lower() == 'done'
        BtsReport.__init__(self, id)

    def _get(self, idx, data):
        if idx in data: return data[idx]
        return []


class BtsSpoolReport(BtsReport):
    """A debbugs report read from the .summary spool file

        tags : bug's tags
        package : 
        srcpackage :
        done :
    """
    
    def __init__(self, id, spooldata):
        self._data = spooldata

        if 'Tags' in spooldata:
            self.tags = spooldata['Tags'].split()
        else:
            self.tags = []

        if 'Forwarded-To' in spooldata:
            self.fwdTo = [x.strip() for x in spooldata['Forwarded-To'].split(', ')]
        else:
            self.fwdTo = []

        self.package    = spooldata['Package']
        self.srcpackage = spooldata['Source']
        self.subject    = spooldata['Subject']
        self.done       = 'Done' in spooldata
        BtsReport.__init__(self, id)


###############################################################################
# BTS Interfaces stuff

class _BtsLdapInterface:
    """Interface with debbugs using the LDAP database"""
    
    dn = "dc=current,dc=bugs,dc=debian,dc=org"

    def __init__(self, ldapurl):
        self.l = ldap.initialize(ldapurl)
        self.l.simple_bind_s()

    def search(self, filter):
        return [i[1] for i in self.l.search_s(self.dn, ldap.SCOPE_ONELEVEL, filter)]

    def getReportOfBzBug(self, bzurl, nb):
        url = bzurl
        filter = "(|(debbugsForwardedTo=%s/%s)(debbugsForwardedTo=%s/show_bug.cgi?id=%s))"
        filter %= (url, nb, url, nb)

        l = self.search(filter)
        if len(l) == 0:
            return None
        else:
            return BtsLdapReport(l[0]['debbugsID'][0], l[0])

    def getReport(self, nb):
        l = self.search('(debbugsID=%s)' % (nb))
        if len(l) == 0:
            return None
        else:
            return BtsLdapReport(nb, l[0])

    def getReportsByFwd(self, *fwds):
        filter = '(|'+''.join(['(debbugsForwardedTo='+s+')' for s in fwds])+')'
        l = self.search(filter)
        return [BtsLdapReport(x['debbugsId'][0], x) for x in l]


class _BtsSpoolInterface:
    """Interface with debbugs using a mirror of its database"""
    
    def __init__(self, spooldir, sourcesfile):
        self._spool = spooldir
        # mapping between binary and source packages 
        self._map   = {}
        with open(sourcesfile) as f:
            for l in f.readlines():
                pkg, _, srcpkg = l.split()
                self._map[pkg] = srcpkg
        f.close()

    def _parsefile(self, id):
        """ converts a bug's .summary file to an array """
        data = {}
        with open(os.path.join(self._spool, id[-2:], id+'.summary')) as f:
            for l in f.readlines():
                k, v = l.split(': ', 1)
                data[k] = v.strip()
        f.close()

        pkg = data['Package'] 
        if pkg in self._map:
            data['Source'] = self._map[pkg]
        else:
            data['Source'] = pkg

        return data

    def getReport(self, bugid):
        try:
            return BtsSpoolReport(bugid, self._parsefile(bugid))
        except:
            return None

    def getReportsByFwd(self, *fwds):
        reg = '|'.join([re.escape(s) for s in fwds])
        cmd = "grep -E '%s' %s/index.fwd" % (reg, self._spool)
        return [self.getReport(l.split(': ', 1)[0]) \
                for l in subprocess.getoutput(cmd).splitlines()]

###############################################################################
# factory

def BtsInterface(cnf):
    if cnf.get('general', 'spool') is not None:
        _parseUserTags(cnf)
        return _BtsSpoolInterface(cnf.get('general', 'spool'), cnf.get('general', 'sources'))
    if cnf.get('general', 'ldap') is not None:
        _parseUserTags(cnf)
        return _BtsLdapInterface(cnf.get('general', 'ldap'))
    return None

